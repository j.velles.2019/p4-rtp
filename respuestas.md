## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?:
* 1268
* ¿Cuánto tiempo dura la captura?:
* 12,81 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?:
* Dividiendo el número de paquetes por el tiempo, obtenemos aproximadamente 99 paquetes/segundo
* ¿Qué protocolos de nivel de red aparecen en la captura?:
* IPV4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?:
* UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?:
* RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N):
* 14
* Dirección IP de la máquina A:
* 192.168.0.10
* Dirección IP de la máquina B:
* 216.234.64.16
* Puerto UDP desde el que se envía el paquete:
* 49154
* Puerto UDP hacia el que se envía el paquete:
* 54550
* SSRC del paquete:
* 0x2a173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):
* ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:
* 4
* ¿Cuántos paquetes hay en el flujo F?:
* 626 paquetes
* ¿El origen del flujo F es la máquina A o B?:
* B
* ¿Cuál es el puerto UDP de destino del flujo F?:
* 49154
* ¿Cuántos segundos dura el flujo F?:
* 12,49 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?:
* 21,18 segundos
* ¿Cuál es el jitter medio del flujo?:
* 0,229 ms
* ¿Cuántos paquetes del flujo se han perdido?:
* 0 paquetes
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:
* 0,5 segundos
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?:
* 26535
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?:
* Pronto, ya que el Timestamp del paquete es menor que el Timestamp del paquete anterior.
* ¿Qué jitter se ha calculado para ese paquete?
* 0,1427 ms
* ¿Qué timestamp tiene ese paquete?
* 1120
* ¿Por qué número hexadecimal empieza sus datos de audio?
* 0x2a173650
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`:
* lun 16 oct 2023 10:46:27 CEST
* Número total de paquetes en la captura:
* 998 paquetes
* Duración total de la captura (en segundos):
* 5,7348 segs
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:
* 450 paquetes
* ¿Cuál es el SSRC del flujo?:
* 0xa6064028
* ¿En qué momento (en ms) de la traza comienza el flujo?
* 2735 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo?
* 12321
* ¿Cuál es el jitter medio del flujo?:
* 0 ms
* ¿Cuántos paquetes del flujo se han perdido?:
* 0 paquetes
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* La captura es de 'pgarcia'
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* Su captura tiene 900 paquetes, por lo que tiene 98 paquetes menos.
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* Su flujo RTP tiene 450 paquetes, por lo que tiene los mismos que mi captura.
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* 20053 - 12321 = 7732
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
* Su flujo RTP empieza en el primer paquete, salvo que el mio empieza en el paquete 99. 
* Esto conlleva a que cambie el valor del puerto, el valor del SSRC y el momento (en segundos) en el que empieza el flujo.
